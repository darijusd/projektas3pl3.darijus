﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Services;
using Hotel.ViewModels;

namespace Hotel.Main
{
    public partial class RoomSave : BaseForm
    {
        private RoomAdminService _roomAdminService;
        private RoomViewModel RoomViewModel;
        public RoomSave(RoomViewModel room = null)
        {
            InitializeComponent();
            _roomAdminService = new RoomAdminService();
            if (room != null)
            {
                FillForm(room);
                RoomViewModel = room;
            } else
            {
                RoomViewModel = new RoomViewModel();
            }
        }

        private void txtRoomNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRoomNumber_Leave(object sender, EventArgs e)
        {
            int roomNumber = 0;
            var txtRoomNumberValue = ((TextBox)sender).Text;
            if (int.TryParse(txtRoomNumberValue, out roomNumber))
            {
                if (_roomAdminService.IsRoomNumberAvailable(roomNumber))
                {
                    lblCheckNumber.Visible = true;
                    lblCheckNumber.Text = "Available.";
                    lblCheckNumber.ForeColor = Color.Green;
                    RoomViewModel.IsRoomNumberAvailable = true;
                }
                else
                {
                    lblCheckNumber.Visible = true;
                    lblCheckNumber.Text = "Not available.";
                    lblCheckNumber.ForeColor = Color.Red;
                    RoomViewModel.IsRoomNumberAvailable = false;
                }
            } else
            {
                MessageBox.Show("Cannot parse room number value.");
                ((TextBox)sender).Clear();
                ((TextBox)sender).Focus();
            }
            
        }
    }
}
