﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.ViewModels;
using Hotel.Services;
using Hotel.Dal.Enumerations;
using Hotel.Main.Utils;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Main
{
    public partial class FinilizeReservation : Form
    {
        private ReservationService _reservationService;
        private ReservationViewModel _reservationViewModel;
        public FinilizeReservation(ReservationViewModel reservationViewModel)
        {
            _reservationService = new ReservationService();
            _reservationViewModel = reservationViewModel;
            InitializeComponent();
            ConfigureGrid();
            FillGrid();
        }

        private void FillGrid()
        {
            var rooms = _reservationService.ReservationRooms(_reservationViewModel);

            foreach (var room in rooms)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["RoomType"].Value = room.RoomType.GetAttribute<DisplayAttribute>().Name;
                dataGridView1.Rows[index].Cells["AccommodationType"].Value = room.AccommodationType.GetAttribute<DisplayAttribute>().Name;
                dataGridView1.Rows[index].Cells["Quantity"].Value = room.Quantity;
                dataGridView1.Rows[index].Cells["Price"].Value = room.FullPrice;
            }
        }

        private void ConfigureGrid()
        {
            DataGridViewTextBoxColumn roomTypeColumn = new DataGridViewTextBoxColumn
            {
                Name = "RoomType",
                HeaderText = "Room Type",
                ReadOnly = true,
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 115
            };
            dataGridView1.Columns.Add(roomTypeColumn);

            DataGridViewTextBoxColumn accommodationTypeColumn = new DataGridViewTextBoxColumn
            {
                Name = "AccommodationType",
                HeaderText = "Accommodation Type",
                ReadOnly = true,
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 150
            };
            dataGridView1.Columns.Add(accommodationTypeColumn);

            DataGridViewTextBoxColumn quantityColumn = new DataGridViewTextBoxColumn
            {
                Name = "Quantity",
                HeaderText = "Quantity",
                ReadOnly = true,
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 50
            };
            dataGridView1.Columns.Add(quantityColumn);

            DataGridViewTextBoxColumn priceColumn = new DataGridViewTextBoxColumn
            {
                Name = "Price",
                HeaderText = "Price",
                ReadOnly = true,
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 50
            };
            dataGridView1.Columns.Add(priceColumn);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            var reservationForm = new Reservation(_reservationViewModel);
            reservationForm.Show();
        }
    }
}
