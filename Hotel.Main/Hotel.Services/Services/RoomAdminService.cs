﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Hotel.ViewModels;
using Hotel.Dal.Enumerations;

namespace Hotel.Services
{
    public class RoomAdminService : BaseService
    {
        public RoomAdminService()
        {

        }

        public string Test()
        {
            try
            {
                OpenConnecion();
                CloseConnection();
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public bool IsRoomNumberAvailable(int roomNumber)
        {
            var checkRoomNumberAvailable = "SELECT COUNT(1) FROM rooms where RoomNumber = @RoomNumber;";
            var cmdCheckRoomNumberAvailable = new MySqlCommand(checkRoomNumberAvailable, hotelConnection);
            cmdCheckRoomNumberAvailable.Parameters.AddWithValue("@RoomNumber", roomNumber);

            OpenConnecion();
            int count = Convert.ToInt32(cmdCheckRoomNumberAvailable.ExecuteScalar());

            CloseConnection();

            return count != 1;
        }

        public List<RoomViewModel> GetRooms()
        {
            List<RoomViewModel> rooms = null ;
            var roomsCmdText = "SELECT * FROM rooms;";
            var roomsCmd = new MySqlCommand(roomsCmdText, hotelConnection);

            OpenConnecion();
            var reader = roomsCmd.ExecuteReader();

            if (reader.HasRows)
            {
                rooms = new List<RoomViewModel>();
            }

            while(reader.Read())
            {
                rooms.Add(new RoomViewModel
                {
                    RoomId = reader.GetInt32("RoomId"),
                    RoomNumber = reader.GetInt32("RoomNumber"),
                    RoomType = (RoomTypeEnum)reader.GetInt32("RoomTypeId"),
                    AdditionalInfo = !reader.IsDBNull(4) ? reader.GetString("AdditionalInfo") : "",
                    IsRoomNumberAvailable = false
                });
            }
            CloseConnection();
            return rooms;
        }

        public TempRoomAddViewModel SaveTempRoom(TempRoomAddViewModel viewModel)
        {
            if(viewModel.Id.HasValue)
            {
                var tempRoomUpdateCmdText = "UPDATE TempRoomAdding SET RoomTypeId = @RoomTypeId, AccommodationTypeId = @accomId, Quantity = @Quantity WHERE Id = @Id;";

                var tempRoomUpdateCmd = new MySqlCommand(tempRoomUpdateCmdText, hotelConnection);
                tempRoomUpdateCmd.Parameters.AddWithValue("@RoomTypeId", viewModel.RoomTypeId);
                tempRoomUpdateCmd.Parameters.AddWithValue("@accomId", viewModel.AccommodationTypeId);
                tempRoomUpdateCmd.Parameters.AddWithValue("@Quantity", viewModel.Quantity);
                tempRoomUpdateCmd.Parameters.AddWithValue("@Id", viewModel.Id);

                OpenConnecion();
                tempRoomUpdateCmd.ExecuteNonQuery();
                CloseConnection();

            } else
            {
                var tempRoomAddCmdText = "INSERT INTO TempRoomAdding (ReservationId, RoomTypeId, AccommodationTypeId, Quantity) VALUES (@ReservationId, @RoomTypeId, @AccommodationTypeId, @Quantity);";

                var tempRoomAddCmd = new MySqlCommand(tempRoomAddCmdText, hotelConnection);
                tempRoomAddCmd.Parameters.AddWithValue("@ReservationId", viewModel.ReservationId);
                tempRoomAddCmd.Parameters.AddWithValue("@RoomTypeId", viewModel.RoomTypeId);
                tempRoomAddCmd.Parameters.AddWithValue("@AccommodationTypeId", viewModel.AccommodationTypeId);
                tempRoomAddCmd.Parameters.AddWithValue("@Quantity", viewModel.Quantity);
                OpenConnecion();
                tempRoomAddCmd.ExecuteNonQuery();
                viewModel.Id = (int)tempRoomAddCmd.LastInsertedId;
                CloseConnection();
                //insert
            }

            return viewModel;
        }
    }
}
