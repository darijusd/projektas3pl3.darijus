﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.Enumerations
{
    public enum RoomTypeEnum
    {
        [Display(Name = "Single")]
        Single = 1,

        [Display(Name = "Double")]
        Double = 2,

        [Display(Name = "Triple")]
        Triple = 3,

        [Display(Name = "Quad")]
        Quad = 4,

        [Display(Name = "Queen")]
        Queen = 5,

        [Display(Name = "King")]
        King = 6,

        [Display(Name = "Twin")]
        Twin = 7,

        [Display(Name = "Hollywood Twin Room")]
        HollywoodTwinRoom = 8,

        [Display(Name = "Double double")]
        DoubleDouble = 9,

        [Display(Name = "Studio")]
        Studio = 10
    }
}
